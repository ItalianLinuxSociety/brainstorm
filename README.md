# Brainstorm

Questo repository è destinato a raccogliere proposte e suggerimenti per attività, iniziative e azioni pratiche a sostegno di Linux e del software libero in Italia. Lo scopo in particolare è quello di abilitare, mettere in relazione, e fornire gli strumenti e le risorse di Italian Linux Society a chi ha una buona idea e vuole empiricamente agire.

Tra le [issue](https://gitlab.com/ItalianLinuxSociety/brainstorm/-/issues) si trovano le proposte già avanzate, coi relativi commenti ed eventualmente i task previsti e lo stato di avanzamento, ed è possibile sottoporre la propria idea.

Per partecipare è necessario avere un account GitLab, [registrabile da qui](https://gitlab.com/users/sign_up).

### Raccomandazioni

- evitare spam nei confronti di progetti inesistenti, sedicenti community costituite da 2 persone, ed in generale iniziative nate ieri e senza un background
- le proposte di presunte "azioni" come petizioni e lettere aperte saranno soppresse nel minor tempo possibile
- le idee e le proposte fini a loro stesse non mancano mai; quel che mancano, spesso, sono le persone che hanno tempo e voglia di implementarle. Se non sei disposto a prenderti in carico la tua stessa proposta, non proporla

Si consiglia inoltre di consultare [la sezione TODO di www.linux.it](https://www.linux.it/todo) per trovare applicazioni e piattaforme già esistenti e consolidate per dare fin da subito il proprio contributo pratico e pragmatico al patrimonio di conoscere libere.
